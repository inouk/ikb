import os

# To allow transparent migration towards ikb 1.0
try:
    from inouk.invoke import task
except:
    from invoke import task


def init(c, part):

    IKB_ENVFILE = os.environ.get('IKB_ENVFILE', None)
    if IKB_ENVFILE:
        try:
            with open(IKB_ENVFILE) as f:
                for _env_line in f.read().splitlines():
                    _var, _value = _env_line.split('=')
                    c.config.buildit[part][_var] = _value
                    #os.environ[_var] = _value
        except:
            print("Failed to read EnvFile: %s " % IKB_ENVFILE)

    for env_var in os.environ:
        c.config.buildit[part][env_var] = os.environ[env_var]
