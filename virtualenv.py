import os
import re
import sys
import pathlib

# To allow transparent migration towards ikb 1.0
try:
    from inouk.invoke import task
    from inouk.invoke.config import DataProxy
except:
    from invoke import task
    from invoke.config import DataProxy

from inouk.buildit import utils  # pylint: disable=import-error


def init(ctx, part):
    global BUILDIT_ROOT_DIRECTORY
    BUILDIT_ROOT_DIRECTORY = ctx.config.buildit['root_directory']

    global RUNNING_ENV_ROOT_DIR
    RUNNING_ENV_ROOT_DIR = ctx.config.buildit.get('running_env_root_dir', BUILDIT_ROOT_DIRECTORY)

    venv_name = ctx.config.buildit[part].name
    venv_path_prefix = ctx.config.buildit[part].get('path_prefix', '')

    print("  virtualenv name            : %s" % venv_name)

    venv_path = os.path.abspath(os.path.join(RUNNING_ENV_ROOT_DIR, venv_path_prefix, venv_name))
    print("  virtualenv path            : %s" % venv_path)

    venv_bin_path = os.path.join(venv_path, "bin")
    print("  virtualenv bin path        : %s" % venv_bin_path)

    interpreter_path = os.path.abspath("%s/python" % venv_bin_path)
    print("  virtualenv interpreter_path: %s" % interpreter_path)

    print("  base interpreter for venv  : %s" % sys.executable)

    # These are exported bindings
    #ctx.config.buildit[part].name = venv_name
    #ctx.config.buildit[part].interpreter = venv_interpreter
    ctx.config.buildit[part].venv_path = venv_path
    ctx.config.buildit[part].venv_bin_path = venv_bin_path
    ctx.config.buildit[part].interpreter_path = interpreter_path

def create_virtualenv(c, part, venv_options:str=None):
    venv_name = c.config.buildit[part].name
    if c.config.buildit[part].interpreter == "python2":
        venv_file = pathlib.Path(venv_name)    
        if not venv_file.exists():
            # create new
            
            # python -m pip install --user virtualenv
            cmd_line = "%s -m pip install virtualenv %s" % (
                c.config.buildit[part].interpreter,
                c.config.buildit[part].venv_path,
            )
            c.run(cmd_line, echo=True, in_stream=False)


            # python -m virtualenv  py2x
            cmd_line = "%s -m virtualenv %s" % (
                c.config.buildit[part].interpreter,
                c.config.buildit[part].venv_path,
            )
            c.run(cmd_line, echo=True, in_stream=False)

    elif c.config.buildit[part].interpreter == "python3":
        venv_file = pathlib.Path(venv_name) 
        # Use current interpreter
        current_interpreter = sys.executable
        if not venv_file.exists():
            cmd_line = "%s -m venv %s %s" % (
                current_interpreter,
                c.config.buildit[part].venv_path,
                venv_options or '',
            )
            c.run(cmd_line, echo=True, in_stream=False)
    else:
        raise Exception(
            "Unsupported value '%s' for 'interpreter' in part:'%s'. Allowed values "\
            "are 'python2', 'python3'." % (
                c.config.buildit[part].interpreter,
                part,
            )
        )

def pip_install(c, part:str, egg_name:str, egg_refspec:any):
    venv_bin_path = c.config.buildit[part].venv_bin_path
    if type(egg_refspec)==str:
        index_url = None
        egg_version = egg_refspec
        editable = None
    elif type(egg_refspec) in (dict, DataProxy,):
        index_url = egg_refspec.get('index-url')
        egg_version = egg_refspec.get('version')
        editable = egg_refspec.get('editable')
        src = egg_refspec.get('src')
    else:
        raise Exception("Unsupported type '%s' for value." % type(egg_refspec))

    index_url = '--index-url=%s ' % index_url if index_url else ''
    egg_version = '==%s' % egg_version if egg_version != '__$latest' else ''
    editable = '--editable=%s ' % editable if editable else ''
    if editable:
        src = "--src=%s" % src if src else ""
        cmd_line = "%s/pip install %s %s" % (venv_bin_path, 
                                             editable,
                                             src)
    else:
        cmd_line = "%s/pip install %s%s%s%s" % (venv_bin_path,
                                                index_url,
                                                editable,
                                                egg_name,
                                                egg_version)

    print(cmd_line)
    c.run(cmd_line, in_stream=False)


def install(ctx, part):

    _venv_options = ctx.config.buildit[part].get('venv_options', None)
    if _venv_options:
        _venv_options = utils.parse_value(ctx, _venv_options)
    else:
        _venv_options = None

    create_virtualenv(ctx, part, venv_options=_venv_options)
    if _venv_options and "--system-site-packages" in _venv_options:
        print("Skipping setuptools and pip install (options=%s)" % _venv_options)
    else:

        egg_refspec = ctx.config.buildit[part].get('setuptools-version')
        if egg_refspec:
            print("Installing setuptools version defined in buildit.json")
            pip_install(ctx, part, 'setuptools', egg_refspec)
        egg_refspec = ctx.config.buildit[part].get('pip-version')
        if egg_refspec:
            print("Installing setuptools version defined in buildit.json")
            pip_install(ctx, part, 'pip', egg_refspec)

        for egg_name in ctx.config.buildit[part].get('eggs', []):
            egg_refspec = ctx.config.buildit[part].eggs[egg_name]
            pip_install(ctx, part, egg_name, egg_refspec)
        

#def update(ctx):
#    print("No update for now")


def reset(c, part):
    venv_name = c.config.buildit[part].name
    c.run("rm -rf %s" % venv_name)
