"Buildit Tasks for Odoo application server"

import configparser
import json
import os
import pathlib
import re
from string import Template
import urllib
import sys

# To allow transparent migration towards ikb 1.0
try:
    from inouk.invoke import task
    from inouk.invoke.config import DataProxy
except:
    from invoke import task
    from invoke.config import DataProxy

from inouk.buildit import utils



ODOO_BUILDIT_CONFIG_FILE = 'odoo.buildit.cfg'
ODOO_BUILDIT_CONFIG_FILE_PATH = None
ODOO_DEFAULT_CONFIG_FILE = 'odoo.default.cfg'
ODOO_DEFAULT_CONFIG_FILE_PATH = None

BUILDIT_ROOT_DIRECTORY = None   # where ikb has been launched
RUNNING_ENV_ROOT_DIR = None
ODOO_DIRECTORY = None
VENV_BIN_PATH = None
ODOO_REQUIREMENTS_NO_DEPS = False
ODOO_LAUNCHER = None

IKB_ODOO_CONFIG_DO_NOT_GENERATE = None


LAUNCHER_V18 = """#!${INTERPRETER_PATH}
import sys

import os 
os.environ['TZ'] = 'UTC'  # set server timezone in UTC before time module imported
if '${VENV_BIN_PATH}' not in os.environ['PATH']:
    os.environ['PATH'] = '%s:%s' % ('${VENV_BIN_PATH}', os.environ['PATH'],)

IKB_DEBUG = os.environ.get('IKB_DEBUG', None)

IKB_ENVFILE = os.environ.get('IKB_ENVFILE', None)
if IKB_ENVFILE:
    try:
        with open(IKB_ENVFILE) as f:
            for _env_line in f.read().splitlines():
                _var, _value = _env_line.split('=')
                os.environ[_var] = _value
    except:
        print("Failed to read EnvFile: %s " % IKB_ENVFILE)

import logging
_logger = logging.getLogger('start_odoo')

# Configuration du logger
_logger.setLevel(logging.INFO)  # Niveau de log

# Création d'un handler pour écrire les logs sur la sortie standard
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)

# Format du log
formatter = logging.Formatter('%(asctime)s %(process)d %(levelname)s %(name)s %(message)s')
handler.setFormatter(formatter)

# Ajout du handler au logger
_logger.addHandler(handler)

BUILDIT_ROOT_DIRECTORY = "${BUILDIT_ROOT_DIRECTORY}"
RUNNING_ENV_ROOT_DIR = "${RUNNING_ENV_ROOT_DIR}"
ODOO_BUILDIT_CONFIG_FILE = "${ODOO_BUILDIT_CONFIG_FILE}"

# For each defined ikb env var, add corresponding command line parameter to sys.argv 
#if it is not already explicitly passed.
# :returns: nothing, update sys.argv

def adjust_params_from_env_vars():
    # Look for a param starting with --ikb-environ-prefix
    env_params = list(filter(lambda p: p.lower().startswith('--ikb-environ-prefix'),sys.argv))
    if env_params:
        for _param in env_params: 
            sys.argv.remove(_param)
        IKB_ENVIRON_PREFIX = env_params[0].split('=')[1]
    else:
        IKB_ENVIRON_PREFIX = None

    ENV_VARS_LIST = {
        "IKB_CONFIG": { 'cl_opts': ['--config','-c'], 'unary': False },
        "IKB_SAVE": { 'cl_opts': ['--save','-s'], 'unary': True },
        "IKB_WITHOUT_DEMO": { 'cl_opts': ['--without-demo'], 'unary': False },
        "IKB_IMPORT_PARTIAL": { 'cl_opts': ['--import-partial','-P'], 'unary': False },
        "IKB_PIDFILE": { 'cl_opts': ['--pidfile'], 'unary': False },
        "IKB_ADDONS_PATH": { 'cl_opts': ['--addons-path'], 'unary': False },
        "IKB_UPGRADE_PATH": { 'cl_opts': ['--upgrade-path'], 'unary': False },
        "IKB_SERVER_WIDE_MODULES": { 'cl_opts': ['--load'], 'unary': False },
        "IKB_DATA_DIR": { 'cl_opts': ['--data-dir','-D'], 'unary': False },
        "IKB_HTTP_INTERFACE": { 'cl_opts': ['--http-interface'], 'unary': False },
        "IKB_HTTP_PORT": { 'cl_opts': ['--http-port','-p'], 'unary': False },
        "IKB_LONGPOLLING_PORT": { 'cl_opts': ['--longpolling-port'], 'unary': False },
        "IKB_HTTP_DISABLE": { 'cl_opts': ['--no-http'], 'unary': True },
        "IKB_PROXY_MODE": { 'cl_opts': ['--proxy-mode'], 'unary': True },
        "IKB_HTTP_INTERFACE": { 'cl_opts': ['--xmlrpc-interface'], 'unary': False },
        "IKB_HTTP_PORT": { 'cl_opts': ['--xmlrpc-port'], 'unary': False },
        "IKB_XMLRPC_DISABLE": { 'cl_opts': ['--no-xmlrpc'], 'unary': True },
        "IKB_DBFILTER": { 'cl_opts': ['--db-filter'], 'unary': False },
        "IKB_TEST_FILE": { 'cl_opts': ['--test-file'], 'unary': False },
        "IKB_TEST_ENABLE": { 'cl_opts': ['--test-enable'], 'unary': False },
        "IKB_TEST_TAGS": { 'cl_opts': ['--test-tags'], 'unary': False },
        "IKB_SCREENCASTS": { 'cl_opts': ['--screencasts'], 'unary': False },
        "IKB_SCREENSHOTS": { 'cl_opts': ['--screenshots'], 'unary': False },
        "IKB_LOGFILE": { 'cl_opts': ['--logfile'], 'unary': False },
        "IKB_SYSLOG": { 'cl_opts': ['--syslog'], 'unary': True },
        "IKB_LOG_HANDLER": { 'cl_opts': ['--log-handler'], 'unary': False },
        "IKB_LOG_REQUEST": { 'cl_opts': ['--log-request'], 'unary': True },
        "IKB_LOG_RESPONSE": { 'cl_opts': ['--log-response'], 'unary': True },
        "IKB_LOG_WEB": { 'cl_opts': ['--log-web'], 'unary': True },
        "IKB_LOG_SQL": { 'cl_opts': ['--log-sql'], 'unary': True },
        "IKB_LOG_DB": { 'cl_opts': ['--log-db'], 'unary': True },
        "IKB_LOG_DB_LEVEL": { 'cl_opts': ['--log-db-level'], 'unary': False },
        "IKB_LOG_LEVEL": { 'cl_opts': ['--log-level'], 'unary': False },
        "IKB_EMAIL_FROM": { 'cl_opts': ['--email-from'], 'unary': False },
        "IKB_SMTP_SERVER": { 'cl_opts': ['--smtp'], 'unary': False },
        "IKB_SMTP_PORT": { 'cl_opts': ['--smtp-port'], 'unary': False },
        "IKB_SMTP_SSL": { 'cl_opts': ['--smtp-ssl'], 'unary': True },
        "IKB_SMTP_USER": { 'cl_opts': ['--smtp-user'], 'unary': False },
        "IKB_SMTP_PASSWORD": { 'cl_opts': ['--smtp-password'], 'unary': False },
        "IKB_DB_NAME": { 'cl_opts': ['--database','-d'], 'unary': False },
        "IKB_DB_USER": { 'cl_opts': ['--db_user','-r'], 'unary': False },
        "IKB_DB_PASSWORD": { 'cl_opts': ['--db_password','-w'], 'unary': False },
        "IKB_PG_PATH": { 'cl_opts': ['--pg_path'], 'unary': False },
        "IKB_DB_HOST": { 'cl_opts': ['--db_host'], 'unary': False },
        "IKB_DB_PORT": { 'cl_opts': ['--db_port'], 'unary': False },
        "IKB_DB_SSLMODE": { 'cl_opts': ['--db_sslmode'], 'unary': False },
        "IKB_DB_MAXCONN": { 'cl_opts': ['--db_maxconn'], 'unary': False },
        "IKB_DB_TEMPLATE": { 'cl_opts': ['--db-template'], 'unary': False },
        "IKB_LOAD_LANGUAGE": { 'cl_opts': ['--load-language'], 'unary': False },
        "IKB_TRANSLATE_MODULES": { 'cl_opts': ['--modules'], 'unary': False },
        "IKB_LIST_DB": { 'cl_opts': ['--no-database-list'], 'unary': False },
        "IKB_DEV_MODE": { 'cl_opts': ['--dev'], 'unary': False },
        "IKB_SHELL_INTERFACE": { 'cl_opts': ['--shell-interface'], 'unary': False },
        "IKB_STOP_AFTER_INIT": { 'cl_opts': ['--stop-after-init'], 'unary': True },
        "IKB_OSV_MEMORY_COUNT_LIMIT": { 'cl_opts': ['--osv-memory-count-limit'], 'unary': False },
        "IKB_OSV_MEMORY_AGE_LIMIT": { 'cl_opts': ['--osv-memory-age-limit'], 'unary': False },
        "IKB_MAX_CRON_THREADS": { 'cl_opts': ['--max-cron-threads'], 'unary': False },
        "IKB_UNACCENT": { 'cl_opts': ['--unaccent'], 'unary': True },
        "IKB_GEOIP_DATABASE": { 'cl_opts': ['--geoip-db'], 'unary': False },
        "IKB_WORKERS": { 'cl_opts': ['--workers'], 'unary': False },
        "IKB_LIMIT_MEMORY_SOFT": { 'cl_opts': ['--limit-memory-soft'], 'unary': False },
        "IKB_LIMIT_MEMORY_HARD": { 'cl_opts': ['--limit-memory-hard'], 'unary': False },
        "IKB_LIMIT_TIME_CPU": { 'cl_opts': ['--limit-time-cpu'], 'unary': False },
        "IKB_LIMIT_TIME_REAL": { 'cl_opts': ['--limit-time-real'], 'unary': False },
        "IKB_LIMIT_TIME_REAL_CRON": { 'cl_opts': ['--limit-time-real-cron'], 'unary': False },
        "IKB_LIMIT_REQUEST": { 'cl_opts': ['--limit-request'], 'unary': False },

        "PGDATABASE": { 'cl_opts': ['--database','-d'], 'unary': False },
        "PGUSER": { 'cl_opts': ['--db_user','-r'], 'unary': False },
        "PGPASSWORD": { 'cl_opts': ['--db_password','-w'], 'unary': False },
        "PGHOST": { 'cl_opts': ['--db_host'], 'unary': False },
        "PGPORT": { 'cl_opts': ['--db_port'], 'unary': False },
        "PGSSLMODE": { 'cl_opts': ['--db_sslmode'], 'unary': False },
        "PGFILTER": { 'cl_opts': ['--db-filter'], 'unary': False },
    }
    for env_var in ENV_VARS_LIST:

        env_var_value = None
        if IKB_ENVIRON_PREFIX:
            env_var_value = os.environ.get("%s_%s" % (IKB_ENVIRON_PREFIX, env_var,))
        if not env_var_value:
            env_var_value = os.environ.get(env_var)

        if env_var_value:
            cl_param_dict = ENV_VARS_LIST[env_var]
            passed = False

            # Check param is passed in long or abrev form
            for cl_param in cl_param_dict['cl_opts']:  
                tmp = list(filter(lambda elem: elem.startswith(cl_param), sys.argv))
                if list(tmp):
                    passed = True
                    break

            # TODO: eval env var to True or False and inject the param or not
            if cl_param_dict['unary']:  # some params don't expect values
                arg_str = "%s" % (cl_param_dict['cl_opts'][0],)
            else:
                arg_str = "%s=%s" % (cl_param_dict['cl_opts'][0], env_var_value,)

            if not passed:
                sys.argv.append(arg_str)
                _logger.info(
                    "'%s' command line parameter added from environment variable '%s'.",  
                    arg_str, env_var
                )
            else:
                _logger.info(
                    "'%s' environment variable value '%s' ignored since '%s' is present in "
                    "command line.",
                    env_var, env_var_value, cl_param_dict['cl_opts'][0],
                )

new_launcher = True
if __name__ == "__main__":
    if new_launcher:  # comprehensively rewrite params to support tools
        arg_is_command = len(sys.argv) > 1 and not sys.argv[1].startswith('-')
        if arg_is_command:
            pass
        else:
            cl_params = sys.argv[1:]
            del sys.argv[1:]
            adjust_params_from_env_vars()
            if not any(map(lambda e: e.startswith('-c') or e.startswith('--config'), sys.argv)):
                sys.argv[1:1] = ['-c', '%s/etc/%s' % (
                    RUNNING_ENV_ROOT_DIR,
                    ODOO_BUILDIT_CONFIG_FILE
                )]
            sys.argv [1:1] = cl_params 
        
        # Reset logging state so that it will be reconfigured based on parameters
        # now they are available
        _logger.info("Command line: %s" % ' '.join(sys.argv))
        rootlogger = logging.getLogger()
        for h in rootlogger.handlers: 
            rootlogger.removeHandler(h)    
        import odoo
        odoo.cli.main()  # main() uses sys.exit so we don't need to return anything 
    else:
        adjust_params_from_env_vars()
        arg_is_command = len(sys.argv) > 1 and not sys.argv[1].startswith('-')
        if not any(map(lambda e: e.startswith('-c') or e.startswith('--config'), sys.argv)):
            if arg_is_command:
                sys.argv[2:2] = ['-c', '%s/etc/%s' % (
                    RUNNING_ENV_ROOT_DIR,
                    ODOO_BUILDIT_CONFIG_FILE
                )]
            else:
                sys.argv[1:1] = ['-c', '%s/etc/%s' % (
                    RUNNING_ENV_ROOT_DIR,
                    ODOO_BUILDIT_CONFIG_FILE
                )]
        
        # Reset logging state so that it will be reconfigured based on parameters
        # now they are available
        rootlogger = logging.getLogger()
        for h in rootlogger.handlers: 
            rootlogger.removeHandler(h)    
        import odoo
        odoo.cli.main()  # main() uses sys.exit so we don't need to return anything 
"""

DEFAULT_ODOO_LAUNCHER_TEMPLATE = """#!${INTERPRETER_PATH}
import sys

import os 
os.environ['TZ'] = 'UTC'  # set server timezone in UTC before time module imported
if '${VENV_BIN_PATH}' not in os.environ['PATH']:
    os.environ['PATH'] = '%s:%s' % ('${VENV_BIN_PATH}', os.environ['PATH'],)

IKB_DEBUG = os.environ.get('IKB_DEBUG', None)

IKB_ENVFILE = os.environ.get('IKB_ENVFILE', None)
if IKB_ENVFILE:
    try:
        with open(IKB_ENVFILE) as f:
            for _env_line in f.read().splitlines():
                _var, _value = _env_line.split('=')
                os.environ[_var] = _value
    except:
        print("Failed to read EnvFile: %s " % IKB_ENVFILE)

import odoo
odoo.netsvc.init_logger()
import logging
_logger = logging.getLogger('start_odoo')

BUILDIT_ROOT_DIRECTORY = "${BUILDIT_ROOT_DIRECTORY}"
RUNNING_ENV_ROOT_DIR = "${RUNNING_ENV_ROOT_DIR}"
ODOO_BUILDIT_CONFIG_FILE = "${ODOO_BUILDIT_CONFIG_FILE}"

# For each defined ikb env var, add corresponding command line parameter to sys.argv 
#if it is not already explicitly passed.
# :returns: nothing, update sys.argv

def adjust_params_from_env_vars():
    # Look for a param starting with --ikb-environ-prefix
    env_params = list(filter(lambda p: p.lower().startswith('--ikb-environ-prefix'),sys.argv))
    if env_params:
        for _param in env_params: 
            sys.argv.remove(_param)
        IKB_ENVIRON_PREFIX = env_params[0].split('=')[1]
    else:
        IKB_ENVIRON_PREFIX = None

    ENV_VARS_LIST = {
        "IKB_CONFIG": { 'cl_opts': ['--config','-c'], 'unary': False },
        "IKB_SAVE": { 'cl_opts': ['--save','-s'], 'unary': True },
        "IKB_WITHOUT_DEMO": { 'cl_opts': ['--without-demo'], 'unary': False },
        "IKB_IMPORT_PARTIAL": { 'cl_opts': ['--import-partial','-P'], 'unary': False },
        "IKB_PIDFILE": { 'cl_opts': ['--pidfile'], 'unary': False },
        "IKB_ADDONS_PATH": { 'cl_opts': ['--addons-path'], 'unary': False },
        "IKB_UPGRADE_PATH": { 'cl_opts': ['--upgrade-path'], 'unary': False },
        "IKB_SERVER_WIDE_MODULES": { 'cl_opts': ['--load'], 'unary': False },
        "IKB_DATA_DIR": { 'cl_opts': ['--data-dir','-D'], 'unary': False },
        "IKB_HTTP_INTERFACE": { 'cl_opts': ['--http-interface'], 'unary': False },
        "IKB_HTTP_PORT": { 'cl_opts': ['--http-port','-p'], 'unary': False },
        "IKB_LONGPOLLING_PORT": { 'cl_opts': ['--longpolling-port'], 'unary': False },
        "IKB_HTTP_DISABLE": { 'cl_opts': ['--no-http'], 'unary': True },
        "IKB_PROXY_MODE": { 'cl_opts': ['--proxy-mode'], 'unary': True },
        "IKB_HTTP_INTERFACE": { 'cl_opts': ['--xmlrpc-interface'], 'unary': False },
        "IKB_HTTP_PORT": { 'cl_opts': ['--xmlrpc-port'], 'unary': False },
        "IKB_XMLRPC_DISABLE": { 'cl_opts': ['--no-xmlrpc'], 'unary': True },
        "IKB_DBFILTER": { 'cl_opts': ['--db-filter'], 'unary': False },
        "IKB_TEST_FILE": { 'cl_opts': ['--test-file'], 'unary': False },
        "IKB_TEST_ENABLE": { 'cl_opts': ['--test-enable'], 'unary': False },
        "IKB_TEST_TAGS": { 'cl_opts': ['--test-tags'], 'unary': False },
        "IKB_SCREENCASTS": { 'cl_opts': ['--screencasts'], 'unary': False },
        "IKB_SCREENSHOTS": { 'cl_opts': ['--screenshots'], 'unary': False },
        "IKB_LOGFILE": { 'cl_opts': ['--logfile'], 'unary': False },
        "IKB_SYSLOG": { 'cl_opts': ['--syslog'], 'unary': True },
        "IKB_LOG_HANDLER": { 'cl_opts': ['--log-handler'], 'unary': False },
        "IKB_LOG_REQUEST": { 'cl_opts': ['--log-request'], 'unary': True },
        "IKB_LOG_RESPONSE": { 'cl_opts': ['--log-response'], 'unary': True },
        "IKB_LOG_WEB": { 'cl_opts': ['--log-web'], 'unary': True },
        "IKB_LOG_SQL": { 'cl_opts': ['--log-sql'], 'unary': True },
        "IKB_LOG_DB": { 'cl_opts': ['--log-db'], 'unary': True },
        "IKB_LOG_DB_LEVEL": { 'cl_opts': ['--log-db-level'], 'unary': False },
        "IKB_LOG_LEVEL": { 'cl_opts': ['--log-level'], 'unary': False },
        "IKB_EMAIL_FROM": { 'cl_opts': ['--email-from'], 'unary': False },
        "IKB_SMTP_SERVER": { 'cl_opts': ['--smtp'], 'unary': False },
        "IKB_SMTP_PORT": { 'cl_opts': ['--smtp-port'], 'unary': False },
        "IKB_SMTP_SSL": { 'cl_opts': ['--smtp-ssl'], 'unary': True },
        "IKB_SMTP_USER": { 'cl_opts': ['--smtp-user'], 'unary': False },
        "IKB_SMTP_PASSWORD": { 'cl_opts': ['--smtp-password'], 'unary': False },
        "IKB_DB_NAME": { 'cl_opts': ['--database','-d'], 'unary': False },
        "IKB_DB_USER": { 'cl_opts': ['--db_user','-r'], 'unary': False },
        "IKB_DB_PASSWORD": { 'cl_opts': ['--db_password','-w'], 'unary': False },
        "IKB_PG_PATH": { 'cl_opts': ['--pg_path'], 'unary': False },
        "IKB_DB_HOST": { 'cl_opts': ['--db_host'], 'unary': False },
        "IKB_DB_PORT": { 'cl_opts': ['--db_port'], 'unary': False },
        "IKB_DB_SSLMODE": { 'cl_opts': ['--db_sslmode'], 'unary': False },
        "IKB_DB_MAXCONN": { 'cl_opts': ['--db_maxconn'], 'unary': False },
        "IKB_DB_TEMPLATE": { 'cl_opts': ['--db-template'], 'unary': False },
        "IKB_LOAD_LANGUAGE": { 'cl_opts': ['--load-language'], 'unary': False },
        "IKB_TRANSLATE_MODULES": { 'cl_opts': ['--modules'], 'unary': False },
        "IKB_LIST_DB": { 'cl_opts': ['--no-database-list'], 'unary': False },
        "IKB_DEV_MODE": { 'cl_opts': ['--dev'], 'unary': False },
        "IKB_SHELL_INTERFACE": { 'cl_opts': ['--shell-interface'], 'unary': False },
        "IKB_STOP_AFTER_INIT": { 'cl_opts': ['--stop-after-init'], 'unary': True },
        "IKB_OSV_MEMORY_COUNT_LIMIT": { 'cl_opts': ['--osv-memory-count-limit'], 'unary': False },
        "IKB_OSV_MEMORY_AGE_LIMIT": { 'cl_opts': ['--osv-memory-age-limit'], 'unary': False },
        "IKB_MAX_CRON_THREADS": { 'cl_opts': ['--max-cron-threads'], 'unary': False },
        "IKB_UNACCENT": { 'cl_opts': ['--unaccent'], 'unary': True },
        "IKB_GEOIP_DATABASE": { 'cl_opts': ['--geoip-db'], 'unary': False },
        "IKB_WORKERS": { 'cl_opts': ['--workers'], 'unary': False },
        "IKB_LIMIT_MEMORY_SOFT": { 'cl_opts': ['--limit-memory-soft'], 'unary': False },
        "IKB_LIMIT_MEMORY_HARD": { 'cl_opts': ['--limit-memory-hard'], 'unary': False },
        "IKB_LIMIT_TIME_CPU": { 'cl_opts': ['--limit-time-cpu'], 'unary': False },
        "IKB_LIMIT_TIME_REAL": { 'cl_opts': ['--limit-time-real'], 'unary': False },
        "IKB_LIMIT_TIME_REAL_CRON": { 'cl_opts': ['--limit-time-real-cron'], 'unary': False },
        "IKB_LIMIT_REQUEST": { 'cl_opts': ['--limit-request'], 'unary': False },

        "PGDATABASE": { 'cl_opts': ['--database','-d'], 'unary': False },
        "PGUSER": { 'cl_opts': ['--db_user','-r'], 'unary': False },
        "PGPASSWORD": { 'cl_opts': ['--db_password','-w'], 'unary': False },
        "PGHOST": { 'cl_opts': ['--db_host'], 'unary': False },
        "PGPORT": { 'cl_opts': ['--db_port'], 'unary': False },
        "PGSSLMODE": { 'cl_opts': ['--db_sslmode'], 'unary': False },
        "PGFILTER": { 'cl_opts': ['--db-filter'], 'unary': False },
    }
    for env_var in ENV_VARS_LIST:

        env_var_value = None
        if IKB_ENVIRON_PREFIX:
            env_var_value = os.environ.get("%s_%s" % (IKB_ENVIRON_PREFIX, env_var,))
        if not env_var_value:
            env_var_value = os.environ.get(env_var)

        if env_var_value:
            cl_param_dict = ENV_VARS_LIST[env_var]
            passed = False

            # Check param is passed in long or abrev form
            for cl_param in cl_param_dict['cl_opts']:  
                tmp = list(filter(lambda elem: elem.startswith(cl_param), sys.argv))
                if list(tmp):
                    passed = True
                    break

            # TODO: eval env var to True or False and inject the param or not
            if cl_param_dict['unary']:  # some params don't expect values
                arg_str = "%s" % (cl_param_dict['cl_opts'][0],)
            else:
                arg_str = "%s=%s" % (cl_param_dict['cl_opts'][0], env_var_value,)

            if not passed:
                sys.argv.append(arg_str)
                _logger.info(
                    "'%s' command line parameter added from environment variable '%s'.",  
                    arg_str, env_var
                )
            else:
                _logger.info(
                    "'%s' environment variable value '%s' ignored since '%s' is present in "
                    "command line.",
                    env_var, env_var_value, cl_param_dict['cl_opts'][0],
                )

if __name__ == "__main__":
    adjust_params_from_env_vars()
    arg_is_command = len(sys.argv) > 1 and not sys.argv[1].startswith('-')
    if not any(map(lambda e: e.startswith('-c') or e.startswith('--config'), sys.argv)):
        if arg_is_command:
            sys.argv[2:2] = ['-c', '%s/etc/%s' % (
                RUNNING_ENV_ROOT_DIR,
                ODOO_BUILDIT_CONFIG_FILE
            )]
        else:
            sys.argv[1:1] = ['-c', '%s/etc/%s' % (
                RUNNING_ENV_ROOT_DIR,
                ODOO_BUILDIT_CONFIG_FILE
            )]
    
    # Reset logging state so that it will be reconfigured based on parameters
    # now they are available
    rootlogger = logging.getLogger()
    for h in rootlogger.handlers: 
        rootlogger.removeHandler(h)    
    odoo.netsvc._logger_init=False     

    odoo.cli.main()
"""


ODOO_LAUNCHER_TEMPLATES = {
    "18" : LAUNCHER_V18
}

def init(c, part):
    """ init is called before all commands """
    global BUILDIT_ROOT_DIRECTORY
    BUILDIT_ROOT_DIRECTORY = c.config.buildit['root_directory']

    global RUNNING_ENV_ROOT_DIR
    RUNNING_ENV_ROOT_DIR = c.config.buildit.get('running_env_root_dir', BUILDIT_ROOT_DIRECTORY)
    
    global ODOO_BUILDIT_CONFIG_FILE_PATH
    ODOO_BUILDIT_CONFIG_FILE_PATH = '%s/etc/%s' % (RUNNING_ENV_ROOT_DIR, ODOO_BUILDIT_CONFIG_FILE,)

    global ODOO_DEFAULT_CONFIG_FILE_PATH
    ODOO_DEFAULT_CONFIG_FILE_PATH = '%s/etc/%s' % (RUNNING_ENV_ROOT_DIR, ODOO_DEFAULT_CONFIG_FILE,)

    global ODOO_DIRECTORY
    odoo_directory = c.config.buildit[part].version.directory
    ODOO_DIRECTORY = "%s/%s" % (RUNNING_ENV_ROOT_DIR, odoo_directory)

    global VENV_BIN_PATH
    python_part = utils.parse_value(c, c.config.buildit[part].python)
    VENV_BIN_PATH = python_part.venv_bin_path

    global ODOO_REQUIREMENTS_NO_DEPS
    ODOO_REQUIREMENTS_NO_DEPS = c.config.buildit[part].requirements.options.get('no-deps', False)

    global ODOO_LAUNCHER
    ODOO_LAUNCHER = c.config.buildit[part].get('launcher_name', 'start_odoo')

    global IKB_ODOO_CONFIG_DO_NOT_GENERATE
    IKB_ODOO_CONFIG_DO_NOT_GENERATE_raw = c.config.buildit[part].get('do_not_generate_config', False)
    #print("IKB_ODOO_CONFIG_DO_NOT_GENERATE=%s  #1" % IKB_ODOO_CONFIG_DO_NOT_GENERATE_raw)
    if IKB_ODOO_CONFIG_DO_NOT_GENERATE_raw:
        #print("IKB_ODOO_CONFIG_DO_NOT_GENERATE=%s  #2" % IKB_ODOO_CONFIG_DO_NOT_GENERATE_raw)
        _tmp = utils.parse_value(c, IKB_ODOO_CONFIG_DO_NOT_GENERATE_raw)
        #print("IKB_ODOO_CONFIG_DO_NOT_GENERATE=%s  #3" % _tmp)
        IKB_ODOO_CONFIG_DO_NOT_GENERATE = (_tmp or '').lower() in ('true', 'yes', '1')
    else:
        IKB_ODOO_CONFIG_DO_NOT_GENERATE = False
    print("IKB_ODOO_CONFIG_DO_NOT_GENERATE=%s" % IKB_ODOO_CONFIG_DO_NOT_GENERATE)

def parse_odoo_pip_requirements(file_path):
    """ Naive pip requirement parser that returns a dict compatible with buildit ``eggs`` configuration.
    :param file_path: path of pip requirements file to parse.
    :returns: dict
    """
    req_dict = {}
    with open(file_path, 'r') as fd:
        for line in fd:
            line_cmp = line.strip().split(';')
            ref_spec = line_cmp[0].strip()
            if len(line_cmp) > 1:
                constraint = line_cmp[1]
            else:
                constraint = ''

            if '==' in ref_spec:
                egg_name, egg_version = ref_spec.split('==') 
            else:
                egg_name = ref_spec
                egg_version = '__$latest'

            if egg_name in req_dict:
                if constraint:
                    if "version" in req_dict[egg_name]:
                        req_dict[egg_name]["version"][egg_version] = constraint
                    else:
                        req_dict[egg_name] = {
                            "version": {
                                egg_version: constraint
                            }
                        }
                else:
                    raise Exception("Unsuported configuration; egg spec mix constrained and unconstrained version specifier.")
            else:
                if constraint:
                    req_dict[egg_name] = {
                        "version": {
                            egg_version: constraint
                        }
                    }
                else:
                    req_dict[egg_name] = egg_version
    return req_dict

def generate_pip_requirements(requirements_dict, file_path):
    with open(file_path, 'w') as fd:    
        for egg_name in requirements_dict:
            req_value = requirements_dict[egg_name]
            if type(req_value) == str:
                if req_value == "__$latest":
                    req_line = egg_name
                else:
                    req_line = "%s==%s" % (egg_name, req_value)
                fd.write("%s\n" % req_line)
            elif type(req_value)==dict:
                for egg_version in req_value['version']:
                    if egg_version=="__$latest":
                        req_line = egg_name
                    else:
                        req_line = "%s==%s" % (egg_name, egg_version)
                    constraint = req_value['version'][egg_version]
                    if constraint:
                        req_line = "%s ; %s"% (req_line, constraint,)
                    fd.write("%s\n" % req_line)
            else:
                raise Exception("Unsupported type")

def install_odoo_requirements_v0(c, part, do_not_install_requirements:bool=False):

    if do_not_install_requirements:
        print( "Skipping Odoo requirements installation." )
        return False
    print("Starting Odoo requirements installation.")

    # extracts req from odoo
    req_dict = parse_odoo_pip_requirements("%s/requirements.txt" % ODOO_DIRECTORY)

    # override with eggs definec in part
    eggs_override = c.config.buildit[part].requirements.get('eggs', {})
    for egg_override in eggs_override:
        if eggs_override[egg_override] is None:
            if egg_override in req_dict:
                del req_dict[egg_override]
        else:
            req_dict[egg_override] = eggs_override[egg_override]

    generate_pip_requirements(req_dict, "%s/buildit-requirements.pip" % ODOO_DIRECTORY)

    # install
    if c.config.buildit[part].requirements.options:
        no_deps = c.config.buildit[part].requirements.options.get('no-deps', False)
    else:
        no_deps = False
    cmd_line = "%s/pip install %s -r %s/buildit-requirements.pip" % (
        VENV_BIN_PATH,
        '--no-deps' if no_deps else '',
        ODOO_DIRECTORY
    )
    result = c.run(cmd_line, warn=False, hide=False, pty=True, echo=True, in_stream=False)
    return

def install_odoo_requirements(c, part, do_not_install_requirements:bool=False):
    """ Install Odoo python requirements
      installs odoo requirements.txt
      installs repository requirements.txt
      then eggs defined in buildit.odoo.requirements.eggs
    """

    if do_not_install_requirements:
        print( "Skipping Odoo requirements installation." )
        return False

    print("Starting Odoo requirements installation.")

    # gather options
    if c.config.buildit[part].requirements.options:
        no_deps = c.config.buildit[part].requirements.options.get('no-deps', False)
    else:
        no_deps = False

    # Install odoo's requirments.txt
    print("Installing Odoo pip requirements.txt")
    cmd_line = "%s/pip install %s -r %s/requirements.txt" % (
        VENV_BIN_PATH,
        '', #'--no-deps' if no_deps else '',
        ODOO_DIRECTORY
    )
    result = c.run(cmd_line, warn=False, hide=False, pty=True, echo=True, in_stream=False)

    # processing pip requirements.txt in repository
    req_file_name = c.config.buildit[part].requirements.get('requirements_file', None)
    if req_file_name:
        print("Looking for repository's pip requirements file: %s" % req_file_name)
        req_path_norm = os.path.normpath(
            os.path.join(BUILDIT_ROOT_DIRECTORY,req_file_name)
        )
        req_file_path = pathlib.Path(req_path_norm)    
        if req_file_path.exists():
            print("Installing repository's pip requirements file: %s" % req_file_path)

            cmd_line = "%s/pip install %s -r %s" % (
                VENV_BIN_PATH,
                '', #'--no-deps' if no_deps else '',
                req_file_path
            )
            result = c.run(cmd_line, warn=False, hide=False, pty=True, echo=True, in_stream=False)
        else:
            print("WARNING: pip requirements file:%s not found. Skipping pip install" % req_file_path)

    # Installing eggs defined in part
    part_eggs_requirements = c.config.buildit[part].requirements.get('eggs', {})
    _buildit_req_file_path = "%s/.ikb/buildit-requirements.pip" % RUNNING_ENV_ROOT_DIR
    if part_eggs_requirements:
        print("Installing eggs defined in buildit.odoo.requirements.eggs")
        generate_pip_requirements(part_eggs_requirements, _buildit_req_file_path)
        cmd_line = "%s/pip install %s -r %s" % (
            VENV_BIN_PATH,
            '--no-deps' if no_deps else '',
            _buildit_req_file_path
        )
        result = c.run(cmd_line, warn=False, hide=False, pty=True, echo=True, in_stream=False)
    else:
        print("Found no eggs to install in buildit.odoo.requirements.eggs")
        cmd_line = "rm -f %s" % _buildit_req_file_path
        result = c.run(cmd_line, warn=False, hide=False, pty=True, echo=True, in_stream=False)

    return

def generate_odoo_launcher(c, part):

    python_part = utils.parse_value(c, c.config.buildit[part].python)
    INTERPRETER_PATH = python_part.interpreter_path
    odoo_major_version = c.config.buildit[part].get('major_version', 'undefined')
    LAUNCHER_TEMPLATE = ODOO_LAUNCHER_TEMPLATES.get(odoo_major_version, DEFAULT_ODOO_LAUNCHER_TEMPLATE)
    
    launcher_tmpl = Template(LAUNCHER_TEMPLATE)
    launcher_script_content = launcher_tmpl.substitute(
        INTERPRETER_PATH=INTERPRETER_PATH, 
        BUILDIT_ROOT_DIRECTORY=BUILDIT_ROOT_DIRECTORY,
        RUNNING_ENV_ROOT_DIR=RUNNING_ENV_ROOT_DIR,
        ODOO_BUILDIT_CONFIG_FILE=ODOO_BUILDIT_CONFIG_FILE,
        VENV_BIN_PATH=VENV_BIN_PATH
    )

    # create bin directory
    cmd_line = "mkdir -p %s/bin" % RUNNING_ENV_ROOT_DIR
    c.run(cmd_line, warn=False, hide=False, pty=True, echo=True, in_stream=False)

    launcher_path = "%s/bin/%s" % (RUNNING_ENV_ROOT_DIR, ODOO_LAUNCHER,)

    with open(launcher_path, 'w') as fd:
        fd.write(launcher_script_content)

    cmd_line = f"chmod +x {launcher_path}"
    c.run(cmd_line, warn=False, hide=False, pty=True, echo=True, in_stream=False)

    _target_install_path = c.config.buildit[part].get('install_into', None)
    if _target_install_path:
        cmd_line = f"sudo ln -fs {launcher_path} {_target_install_path}"
        c.run(cmd_line, warn=False, hide=False, pty=True, echo=True, in_stream=False)

    return

def generate_odoo_config(c, part):
    """ 
    Generates odoo.cfg used by start_odoo.
    Odoo has already been installed when this is called.
    """
    if IKB_ODOO_CONFIG_DO_NOT_GENERATE:
        print("Skipping Odoo config generation")
        return False

    print("Generating base configuration (using odoo).")

    # delete existing template
    cmd_line = 'rm -rf %s' % ODOO_DEFAULT_CONFIG_FILE_PATH
    c.run(cmd_line, warn=False, hide=False, pty=True, echo=True, in_stream=False)

    # launch odoo to generate a default config in etc/odoo.default.cfg
    cmd_line = '%(venv_bin)s/odoo --config=%(def_cfg_path)s --save --stop-after-init' % {
        'venv_bin': VENV_BIN_PATH,
        'def_cfg_path': ODOO_DEFAULT_CONFIG_FILE_PATH
    }
    c.run(cmd_line, warn=False, hide=False, pty=True, echo=True, in_stream=False)
    
    # parse odoo default cfg file into a dict
    config = configparser.ConfigParser(interpolation=None)
    config.read(ODOO_DEFAULT_CONFIG_FILE_PATH)
    
    # Configure addons_path
    # we merge in this order:
    #   - odoo_default_addons_path
    #   - c.config.buildit[part].addons_path_dict <=> installed addons paths
    #   - c.config.buildit[part].addons_path
    odoo_default_addons_path_str = config['options'].get('addons_path','')
    odoo_default_addons_path = odoo_default_addons_path_str.split(',')

    buildit_generated_addons_path = list(c.config.buildit[part].addons_path_dict.keys())
    
    config_addons_path = c.config.buildit[part].get('addons_path', [])
    if config_addons_path:
        if isinstance(config_addons_path, str):
            config_addons_path = [config_addons_path]

    addons_path = odoo_default_addons_path
    for path in buildit_generated_addons_path:
        if path not in addons_path:
            addons_path.append(path)

    for path in config_addons_path:
        if path not in addons_path:
            addons_path.append(path)
    config['options']['addons_path'] = ','.join(addons_path)

    # server_wide_modules
    _odoo_server_wide_modules_str = config.get('options', 'server_wide_modules', fallback=None)
    print("Odoo defined server_wide_modules=%s" % _odoo_server_wide_modules_str)
    if _odoo_server_wide_modules_str:
        _odoo_server_wide_modules = _odoo_server_wide_modules_str.split(',')
    else:
        _odoo_server_wide_modules = []

    _ikb_server_wide_addons = []
    for _addon_name in c.config.buildit[part].get('addons', {}):
        _addon = c.config.buildit[part]['addons'][_addon_name]
        if _addon.get('server_wide') and _addon.get('install'):
            if _addon_name not in _ikb_server_wide_addons:
                _ikb_server_wide_addons.append(_addon_name)
    if _ikb_server_wide_addons:
        print("ikb defined server_wide_modules=%s", _ikb_server_wide_addons)
        _final_server_wide_addons = _odoo_server_wide_modules + _ikb_server_wide_addons
        print("Final server_wide_modules=%s" % _final_server_wide_addons)
        config['options']['server_wide_modules'] = ','.join(_final_server_wide_addons)
        print("Final (config) server_wide_modules='%s'" % config['options']['server_wide_modules'])

    # Override Odoo default config with options defined 
    # in buildit.odoo.config options.
    config_override_dict = c.config.buildit[part].get('config', {})
    for section in config_override_dict:
        if section not in config:
            config[section]={}
        for key in config_override_dict[section]:
            if key == 'server_wide_modules':
                print("WARNING: DEPRECATED: server_wide_modules is defined in buildit.odoo.config.options. Use `server_wide`: option in `buildit.odoo.config.addons` instead.")
            raw_value = config_override_dict[section][key]
            parsed_value = utils.parse_value(c, raw_value)
            if not isinstance(parsed_value, str):
                parsed_value = str(parsed_value)
            config[section][key] = parsed_value
    
    # generate odoo config file
    with open(ODOO_BUILDIT_CONFIG_FILE_PATH, 'w') as buildit_cfg_file:
        config.write(buildit_cfg_file)

    return

def install_odoo(c, part, do_not_install_requirements:bool=False):
    """Install odoo via pip
    
    Please note that we use c.config[part].requirements.options.no-deps
    """
    if do_not_install_requirements or  (ODOO_REQUIREMENTS_NO_DEPS and '--no-deps' in ODOO_REQUIREMENTS_NO_DEPS):
        no_deps = '--no-deps'
    else:
        no_deps = ''

    cmd_line = "%s/pip install %s -e %s" % (
        VENV_BIN_PATH,
        no_deps,
        ODOO_DIRECTORY,
    )
    c.run(cmd_line, warn=False, hide=False, pty=True, echo=True, in_stream=False)

def clone_odoo(c, part):    
    version_type = c.config.buildit[part].version.type  # git, file, ...
    venv_bin = utils.parse_value(c, c.config.buildit[part].python).venv_bin_path

    if version_type in ('git',):
        with c.cd(BUILDIT_ROOT_DIRECTORY):
            repository = c.config.buildit[part].version.repository
            depth = c.config.buildit[part].version.get('depth')
            branch = c.config.buildit[part].version.get('branch')
            refspec = c.config.buildit[part].version.get('refspec')
            if depth and not branch:
                err_msg = "Missing required buildit.%s.version.branch when depth is specified." % part
                print(err_msg)
                raise Exception(err_msg)

            cmd_line = "git clone --quiet %(depth)s %(branch)s %(repository)s %(directory)s" % {
                "depth": "--depth=%s" % depth if depth else "",
                "branch": "--branch=%s" % branch if branch else "",
                "repository": repository,
                "directory":  ODOO_DIRECTORY
            }
            result = c.run(cmd_line, warn=True, hide=False, pty=True, echo=True, in_stream=False)
            with c.cd(ODOO_DIRECTORY):
                c.run("git fetch", echo=True, pty=True, in_stream=False)
                if refspec:
                    c.run("git checkout %s" % refspec, warn=True, pty=True, echo=True, in_stream=False)
                    c.run("git pull", warn=True, pty=True, echo=True, in_stream=False)
                else:
                    raise Exception("Missing required buildit.%s.version.refspec." % part)
    else:
        raise Exception("Unsupported version type: '%s'" % version_type)

def compute_repo_token(c, part, repository, addon_name):
    """ For https repos with a token key, compute an adjusted url
    :return token for repository if any is defined or None

    Token can be specified using 2 selectors:
     - "token": "{{servernanme}}"
     - "token": "{{tokenname}}@{{servernanme}}"


    """
    if not "token" in repository:
        return None

    if not repository.repository.startswith('https'):
        raise Exception(
            f"Repository '{addon_name}'. Cannot use token with non https repository URL."
        )

    if '@' in repository.token:
        token_name, token_provider = repository.token.split('@')
    else:
        token_provider = repository.token
        token_name = None

    if not c.config.buildit.get('git_tokens', {}):
        raise Exception("No 'git_tokens' defined in buildit config.")

    tokens_dict = c.config.buildit.git_tokens.get(token_provider, None)
    if not tokens_dict:
        raise Exception(
            f"No tokens defined for provider:'{token_provider}' in 'buildit.git_tokens'"
        )

    if not token_name:
        token_name = next(iter(tokens_dict)) 

    if token_name not in tokens_dict:
        raise Exception(
            f"Token '{token_name}' do not exist in buildit.git_tokens.{token_provider}"
        )
    token_value = tokens_dict[token_name]['token']
    interpolated_token_value = utils.parse_value(c, token_value)
    return interpolated_token_value    

def compute_repo_url(c, part, repository, addon_name):
    """ Inject a token like "user:pwd" into repository url which must https.
    :returns: URL with token inside
    """
    if not "token" in repository:
        return repository.repository
    token_value = compute_repo_token(c, part, repository, addon_name)
    if not token_value:
        return repository.repository

    _url_parts = urllib.parse.urlparse(repository.repository)
    _url_parts = list(_url_parts)
    _url_parts[1] = f"{token_value}@{_url_parts[1]}"

    # urlunsplit accepts list
    repo_url = urllib.parse.urlunsplit(_url_parts[:5])
    return repo_url

def git_clone(c, part, repository, addon_name='undefined', target_directory=None):
    """ 
    """
    with c.cd(RUNNING_ENV_ROOT_DIR):
        depth = repository.get('depth')
        refspec = repository.get('refspec')
        repo_url_raw = compute_repo_url(c, part, repository, addon_name)
        repo_url = utils.parse_value(c, repo_url_raw)
        cmd_line = "git clone --quiet %(depth)s %(repository)s %(target_directory)s" % {
            "depth": "--depth=%s" % depth if depth else "",
            "repository": repo_url,
            "target_directory": target_directory
        }
        result = c.run(cmd_line, warn=True, hide=False, pty=True, echo=True, in_stream=False)

        with c.cd(target_directory):
            c.run("git fetch", warn=False, hide=False, pty=True, echo=True, in_stream=False)
            if refspec:
                c.run("git checkout %s" % refspec, warn=False, hide=False, pty=True, echo=True, in_stream=False)
                _r = c.run("git symbolic-ref --quiet HEAD", warn=True, hide=False, pty=True, echo=True, in_stream=False)
                if _r.ok:  # On a branch
                    result = c.run("git pull", warn=False, hide=False, pty=True, echo=True, in_stream=False)
                else:  # detached: tag, commit, ...
                    pass
    return target_directory

def clone_addons(c, part):
    """ Process addons list and launch retreival of git and http addons
    :returns: addons_path_dict containing all addon path and insert addons_path_dict in 
              c.config.buildit[part]
    """
    addons_path_dict = {}  # eg. {'/opt/muppy/buildit-running-env/parts/inouk_addons': ['inouk_core', 'inouk_session_store', 'inouk_message_queue']}

    for addon_name in c.config.buildit[part].addons:
        addon = c.config.buildit[part].addons[addon_name]
        if addon.get('do_not_install', False):
            print("Skipping addon: '%s' having do_not_install=true (replace by install=false)" % addon_name)
            continue
        if not addon.get('install', True):
            print("Skipping addon: '%s' having install=false" % addon_name)
            continue

        # Define where we will checkout 
        # Allowed values:
        #   - 'running-env-root-path'
        #   - 'repository' (where ikb install has been launched)

        _root_folder = addon.get('root_folder', 'running-env-root-path') 
        if _root_folder=='repository':  # target = BUILDIT_ROOT_DIRECTORY
            CHECKOUT_ROOT_FOLDER = BUILDIT_ROOT_DIRECTORY
        else:
            CHECKOUT_ROOT_FOLDER = RUNNING_ENV_ROOT_DIR

        print("Installing addon_name=%s" % addon_name)
        if addon.type == 'local':
            addon_path_norm = os.path.normpath(os.path.join(CHECKOUT_ROOT_FOLDER, addon.path))
            addon_path = pathlib.Path(addon_path_norm)    
            if not addon_path.exists():
                raise Exception("'%s' is declared as addon path, but it does not exists!!!!" % addon_path)
            print("Adding: '%s' to addons_path." % addon_path_norm)
            addons_path_dict[addon_path_norm] = [addon_name]

        elif addon.type == 'git':
            if addon.get('group'):
                addon_directory = "%s/%s" % (CHECKOUT_ROOT_FOLDER, addon['group'],)
                target_directory = "%s/%s" % (addon_directory, addon.directory)
            else:
                addon_directory = "%s/%s" % (CHECKOUT_ROOT_FOLDER, addon.directory,)
                target_directory = addon_directory

            # normalize to accomodate difference in builit.json group spelling
            addon_directory = os.path.normpath(addon_directory)

            if addon_directory in addons_path_dict:
                addons_path_dict[addon_directory].append(addon_name)
            else:
                addons_path_dict[addon_directory] = [addon_name]

            git_clone(
                c, part,
                addon,
                addon_name,
                target_directory=target_directory
            )

        elif addon.type == 'http_file':
            # "addons": {
            #     "theme_treehouse": {
            #         "type": "http_file",
            #         "uri": "https://www.dropbox.com/s/szwxc96legsgclq/theme_treehouse-13.0.1.0.zip?dl=1",
            #         "directory": "parts/themes",
            #         "#file_name": "theme_treehouse-13.0.1.0.zip",
            #         "#group": "parts/inouk_addons"
            #     }
            # }
            # Note: 
            #   * Unzipped directory must contrain addon sub directories. Not one addon content.
            #   * Since addons_directory is erased at each install it must be unique.
            addons_directory = "%s/%s" % (CHECKOUT_ROOT_FOLDER, addon['directory'],)
            cmd_line = 'rm -rf %s' % addons_directory
            c.run(cmd_line)

            cmd_line = 'mkdir -p %s' % addons_directory
            c.run(cmd_line)

            # use supplied file_name or try to extract it from server
            file_name = addon.get('file_name')
            if not file_name:
                try:
                    cmd_line = "curl -sIkL %s | grep 'filename='" % addon['uri']
                    result = c.run(cmd_line, warn=False, hide=False, pty=True, echo=True, in_stream=False)
                    file_name = re.search(r'filename=\"(?P<file_name>.*)\"', result.stdout).group('file_name')
                except:
                    raise Exception("Failed to extract filename from %s. You must provide a 'file_name' property in addon '%s'." % addon['uri'], addon_name)
            
            with c.cd(addons_directory):
                cmd_line = "curl -JL %s -o %s" % (addon['uri'], file_name,)
                result = c.run(cmd_line, warn=False, hide=False, pty=True, echo=True, in_stream=False)
                ext = os.path.splitext(file_name)[1]
                if ext=='.zip':
                    cmd_line = "unzip %s" % (file_name,)
                    result = c.run(cmd_line)
                else:
                    raise Exception("'%s' files are not supported yet." % ext)

            if addons_directory in addons_path_dict:
                addons_path_dict[addons_directory].append(addon_name)
            else:
                addons_path_dict[addons_directory] = [addon_name]

    c.config.buildit[part]['addons_path_dict'] = addons_path_dict
    print("Final addons_path_dict: %s" % addons_path_dict)
    return addons_path_dict

def install(c, part):

    clone_odoo(c, part)
    clone_addons(c, part)

    # install eggs from odoo/requirements.txt updated with eggs config from odoo part
    do_not_install_requirements_raw = c.config.buildit[part].requirements.get('do_not_install', None)
    do_not_install_requirements = utils.parse_value(c, do_not_install_requirements_raw)

    install_odoo_requirements(c, part, do_not_install_requirements)
    
    # Install odoo in editable mode
    install_odoo(c, part, do_not_install_requirements)

    generate_odoo_config(c, part)

    generate_odoo_launcher(c, part)

#def update(ctx, part):
#    No update for now
#    pass

def reset(c, part):
    directory = c.config.buildit[part].version.directory
    #c.run("rm -rf %s" % directory)

    # We no longer rm bin since it can be used to store user binaries
    #cmd_line = "rm -rf %s/bin" % BUILDIT_ROOT_DIRECTORY
    #c.run(cmd_line)

    cmd_line = "rm -rf %s/etc" % RUNNING_ENV_ROOT_DIR
    c.run(cmd_line)

    # Clean launcher
    _launcher_path = "%s/bin/%s" % (RUNNING_ENV_ROOT_DIR, ODOO_LAUNCHER,)
    print("Deleting '%s'" % _launcher_path)
    cmd_line = f"sudo rm -rf {_launcher_path}"
    c.run(cmd_line, warn=False, hide=False, pty=True, echo=True, in_stream=False)

    _target_install_path = c.config.buildit[part].get('install_into', None)
    if _target_install_path:
        _path_launcher = os.path.join(_target_install_path, ODOO_LAUNCHER)
        print("Deleting %s" % _path_launcher)
        cmd_line = f"sudo rm -rf {_path_launcher}"
        c.run(cmd_line, warn=False, hide=False, pty=True, echo=True, in_stream=False)

    # TODO: Clean
