""" Buildit Tasks for systemd service setup 
DEPRECATED: Do not use
"""
from string import Template

# To allow transparent migration towards ikb 1.0
try:
    from inouk.invoke import task, Collection
    from inouk.invoke.config import DataProxy
except:
    from invoke import task
    from invoke.config import DataProxy


from inouk.buildit import utils  # pylint: disable=import-error
from inouk.buildit.tasks import custom_task_init  # pylint: disable=import-error

# pylint: disable=invalid-name

SYSTEMD_UNIT_FILE_TEMPLATE = Template("""[Unit]
${DESCRIPTION}

[Service]
${USER}
${GROUP}

#Environment=ONE='one' "TWO='two two' too" THREE=
${ENVIRONMENT}
Type=${TYPE}
${EXECSTART}
${WORKING_DIRECTORY}
# Any of no, always, on-success, on-failure, on-abnormal, on-abort, on-watchdog
# Default = no
Restart=${RESTART}

[Install]
WantedBy=multi-user.target
""")


# pylint: disable=unused-argument,missing-function-docstring
def init(c, part):
    pass


@task(help={'ExecStart_args': "ExecStart parameters as a str."})
def setup_service(c, ExecStart_args=None):
    """Setup systemd units defined using plugin 'ikb_plugins.systemd'.

Example json configuration:

        "mpy_pglookout.service": {
            "plugin": "ikb_plugins.systemd",
            "Description": "Muppy pglookout service",
            "ExecStart": "${py3x:venv_bin_path}/pglookout",
            "//ExecStart_args": "--config=\"https://dev.cyril-dev.muppy.ovh/mpy/api/v1/pgha_pglookout_config?muppy-static-token=hohoho&rcs_name=ha\"",
            "Restart": "always",
            "User": "cmorisse",
            "//Group": "cmorisse",
            "Type": "simple",
            "WorkingDirectory": "${root_directory}",
            "//RootDirectory": "${}",
            "//Environment": {
                "MPY_MONITOR_CONFIG": "\"https://dev.cyril-dev.muppy.ovh/mpy/api/v1/pgha_pglookout_config?muppy-static-token=hohoho&rcs_name=ha\""
            },
        }
"""
    parts = custom_task_init(c, 'ikb_plugins.systemd')

    for part_name in parts:
        print("Setup systemd service for part:'%s'" % part_name)

        tmpl_context = {}
        part = parts[part_name]

        ExecStart_parsed = utils.parse_value(c, part.ExecStart)
        EXECSTART = "ExecStart=%s" % ExecStart_parsed
        # ExecStart_args param precedes buildit settings
        if ExecStart_args is None:
            if part.get('ExecStart_args'):
                ExecStart_args = part.ExecStart_args
        if ExecStart_args:
            EXECSTART += " %s" % (ExecStart_args)
        tmpl_context['EXECSTART'] = EXECSTART

        if part.get('Environment'):
            ENVIRONMENT = ' '.join(
                ['%s=%s' % (key, val,) for (key, val,) in part.Environment.items()]
            )
            tmpl_context['ENVIRONMENT'] = "Environment=%s" % ENVIRONMENT
        else:
            tmpl_context['ENVIRONMENT'] = ''

        if part.get('Description'):
            tmpl_context['DESCRIPTION'] = "Description=%s" % part.Description
        else:
            tmpl_context['DESCRIPTION'] = ''

        if part.get('User'):
            tmpl_context['USER'] = "User=%s" % utils.parse_value(c, part.User)
        else:
            tmpl_context['USER'] = ''

        if part.get('Group'):
            tmpl_context['GROUP'] = "Group=%s" % utils.parse_value(c, part.Group)
        else:
            tmpl_context['GROUP'] = ''

        if part.get('WorkingDirectory'):
            tmpl_context['WORKING_DIRECTORY'] = "WorkingDirectory=%s" % utils.parse_value(c, part.WorkingDirectory)
        else:
            tmpl_context['WORKING_DIRECTORY'] = ''

        if part.get('RootDirectory'):
            tmpl_context['RootDirectory'] = "RootDirectory=%s" % utils.parse_value(c, part.RootDirectory)
        else:
            tmpl_context['RootDirectory'] = ''

        RESTART_ALLOWED_VALUES = ('no', 'always', 'on-success', 'on-failure', 'on-abnormal', 'on-abort', 'on-watchdog',)
        if part.get('Restart'):
            if part.Restart not in RESTART_ALLOWED_VALUES:
                raise Exception("Wrong value:'%s' for Restart must be one of: %s" % (part.Restart, ', '.join(RESTART_ALLOWED_VALUES),))
            tmpl_context['RESTART'] = part.Restart
        else:
            tmpl_context['RESTART'] = 'no'

        TYPE_ALLOWED_VALUES = ('simple', 'exec', 'forking', 'oneshot', 'dbus', 'notify', 'idle',)
        if part.get('Type'):
            if part.Type not in TYPE_ALLOWED_VALUES:
                raise Exception("Wrong value:'%s' for 'Type'. 'Type' must be one of: %s" % (
                    part.Type, ', '.join(TYPE_ALLOWED_VALUES),
                ))
            tmpl_context['TYPE'] = part.Type
        else:
            tmpl_context['TYPE'] = 'simple'

        # Genère le fichier
        system_unit_file_content = SYSTEMD_UNIT_FILE_TEMPLATE.substitute(tmpl_context)
        c.run('mkdir -p buildit-services', in_stream=False)
        with open('buildit-services/%s' % part_name, 'w') as fh:
            fh.write(system_unit_file_content)

        # créer un lien dans //etc/systemd/
        cmd_line = "sudo ln -fs buildit-services/%s /etc/systemd/system" % part_name
        print("\n\nWarning!!!\n\nSystem may ask to enter your password as ikb tries to execute:\n    '%s'\n" % cmd_line)
        c.run("sudo ln -fs %s/buildit-services/%s /etc/systemd/system" % (c.buildit.root_directory, part_name,), in_stream=False)
        c.run("sudo systemctl daemon-reload", in_stream=False)
        print("Systemd service:'%s' created." % part_name)


#def reset(c, part):
#    pass
