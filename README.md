
# `ikb` Odoo Plugin Documentation

The `ikb` Odoo plugin facilitates the management and installation of all Odoo versions. It provides the following functionalities:

- **Checkout Odoo:** Automatically fetches the desired version of Odoo from the appropriate source.
- **Checkout Dependency Addons:** Retrieves the required addons that Odoo depends on.
- **Generate the `odoo.cfg` File:**
  - Configures the `addons_path` to include all necessary addons.
  - Adjusts the `server_wide_modules` setting to account for addons managed by the plugin.
- **Generate a Launcher File:**
  - Creates a launcher file for simplified execution.
  - Optionally makes the launcher available in the system's `PATH` for easy access.

The installation location for Odoo and its addons depends on the value of the `buildit.running-env-root-path` key.

## Plugin configuration options example

```jsonc
{
    "buildit": {
        "running-env-root-path": "../buildit-running-env",
        "odoo": {
            "plugin": "ikb.odoo",
            "python": "${py3x}",
            "launcher_name": "odoo-ikb",        // By default launcher is generated in buildit-running-env/bin
            "install_into": "/usr/local/bin/",  // Use only if you want to create a link outside buildit-running-env/bin
            "major_version": "18",
            "version": {
                "type": "git",
                "repository": "https://github.com/odoo/odoo.git",
                "depth": 5,
                "directory": "odoo18",
                "refspec": "18.0",
                "branch": "18.0"
            },
            "requirements": {
                "options": {
                    "no-deps": false
                },
                "eggs": {}
            },
            /*
             * use addons to checkout addons and add them to addons_path
             */
            "addons": {
                "project_addons": {
                    "install": true,
                    "type": "local",                // local addons 
                    "root_folder": "repository",    // "repository" => path relative to , null => Running env root folder 
                    "path": "."                     // path is relative to maib repo root (CWD)
                },
                /*
                 * enterprise is a group of addons so directory is added to addon_path
                 */
                "enterprise": {                  // Note the use of ENV Var for auth
                    "install": false,
                    "type": "git",
                    //"root_folder": "running-env-root-path",  // default is  path relative to "running-env-root-path"
                    "repository": "https://${ENV:MPY_GITHUB_DEPS_TOKEN_URL_AUTH}github.com/odoo/enterprise.git",
                    "directory": "parts/enterprise",
                    "refspec": "18.0"
                },
                /*
                 * inouk_core (and others below) are single addon.
                 * We use group to define a single directory where they are installed.
                 * Only the group directory is added to addons_path.
                 */
                "inouk_core": {
                    "install": true,
                    "type": "git",
                    "repository": "https://gitlab.com/cmorisse/inouk_core.git",
                    "directory": "inouk_core",
                    "group": "parts/inouk_addons",   // final directory is parts/inouk_addons/inouk_core
                    "depth": 5,         // use depth to limit volume of what is cloned.
                    "refspec": "16.0",  // use refspec to defines what must be checked out
                    "branch": "16.0",   // branch is used at clone to clone a specific branch else default branch is cloned
                },
                "inouk_api_auth": {
                    "install": false,
                    "type": "git",
                    "repository": "https://github.com/cmorisse/inouk_api_auth.git",
                    "directory": "inouk_api_auth",
                    "refspec": "13.0",
                    "group": "parts/inouk_addons"
                },
                "inouk_session_store": {
                    "install": true,
                    "server_wide": true,
                    "type": "git",
                    "repository": "https://github.com/cmorisse/inouk_session_store.git",
                    "directory": "inouk_session_store",
                    "refspec": "16.0",
                    "group": "parts/inouk_addons"
                },
                "inouk_attachments_storage": {
                    "install": false,
                    "type": "git",
                    "repository": "https://github.com/cmorisse/inouk_attachments_storage.git",
                    "directory": "inouk_attachments_storage",
                    "refspec": "16.0",
                    "group": "parts/inouk_addons"
                },
                "inouk_message_queue": {
                    "install": true,
                    "type": "git",
                    "repository": "https://gitlab.com/cmorisse/inouk_message_queue.git",
                    "directory": "inouk_message_queue",
                    "refspec": "16.0",
                    "group": "parts/inouk_addons"
                },
                "inouk_oauth": {
                    "install": false,
                    "type": "git",
                    "repository": "https://buildit:${ENV:MPY_REPO_GIT_TOKEN}@gitlab.com/inouk/inouk_oauth.git",
                    "directory": "inouk_oauth",
                    "refspec": "16.0",
                    "group": "parts/inouk_addons"
                },
                "inouk_server_detect": {
                    "install": false,
                    "type": "git",
                    "repository": "https://gitlab.com/cmorisse/inouk_server_detect.git",
                    "directory": "inouk_server_detect",
                    "refspec": "16.0",
                    "group": "parts/inouk_addons"
                },
                "inouk_odoo_pg_health_check": {
                    "install": false
                },
                "inouk_odoo_data_migration_toolkit": {
                    "install": false
                },                
            },
            /*
             * DEPERECATED option to add absolute paths to addons_path
             * use addon of type 'local'
             */
            "addons_path": [     
                "absolute_path_to_add_to_addon_path1",
                "absolute_path_to_add_to_addon_path2"
            ],
            "config": {
                "options": {
                    "admin_passwd": "${ENV:IKB_ODOO_ADMIN_PASSWORD}",
                    "db_maxconn": 256,
                    "list_db": true,
                    "unaccent": false,
                    "proxy_mode": true,
                    "http_interface": "0.0.0.0",
                    "limit_time_cpu": 360,
                    "limit_time_real": 720,
                    "limit_time_real_cron": 0,
                    "limit_request": 8192,
                    "max_cron_threads": 2,
                    "log_handler": ":INFO,IMQWorker:INFO",
                    "log_db": false,
                    "log_db_level": "warning",
                    "http_port": 8069,
                    "gevent_port": 8072,
                    "db_sslmode": "prefer"
                    //"server_wide_modules": "base,web"
                }
            }
        }
    }
}
```

### server_wide_modules

The Odoo `server_wide_modules` configuration option is set as follows by `ìkb`:
	•	The default value is defined by Odoo (extracted from the default configuration generated by Odoo).
	•	`ìkb` adds the addons that include the “server_wide” option (see the inouk_session_store example above).

**Important**: If a value is specified in the buildit.odoo.config.options.server_wide_modules key within the Buildit file, it will override everything.

# `ikb` environ Plugin Documentation

This plugin injects all environment variables into a `part` and allows them to be used as `values`.

## Example

```yaml
#
# Declare a `part` named ENV that will contain all Environment VARS in "base" buildit.json: appserver.buildir.json
{
    "buildit": {
        "buildit-plugins": {
            "plugin": "inouk.buildit.git_checkout",
            "repositories": {
                "core-buildit-plugins": {
                    "w_repository": "git@gitlab.com:inouk/ikb.git",
                    "r_repository": "http://gitlab.com/inouk/ikb.git",
                    "repository": "http://gitlab.com/inouk/ikb.git",
                    "directory": "ikb",
                    "refspec__disabled": null,
                    "update": true
                }
            }
        },
        "ENV": {
            "plugin": "ikb.environ"
        },
        ...
    }
}

# Use an ENV Var in buildit.jon using values syntax: "${PARTNAME:VARNAME}"
{
    "__$extends": "appserver.buildit.json",
    "buildit": {
        "odoo": {
            "config": {
                "options": {
                    "admin_passwd": "${ENV:BUILDIT_ADMIN_PASSWORD}",   
    ...

```

